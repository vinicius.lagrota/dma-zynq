--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Command: generate_target bd_36cd.bd
--Design : bd_36cd
--Purpose: IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_36cd is
  port (
    SLOT_0_AXIS_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_0_AXIS_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_0_AXIS_tlast : in STD_LOGIC;
    SLOT_0_AXIS_tready : in STD_LOGIC;
    SLOT_0_AXIS_tstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_0_AXIS_tvalid : in STD_LOGIC;
    SLOT_1_AXIS_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_1_AXIS_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    SLOT_1_AXIS_tlast : in STD_LOGIC;
    SLOT_1_AXIS_tready : in STD_LOGIC;
    SLOT_1_AXIS_tvalid : in STD_LOGIC;
    SLOT_2_BRAM_addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    SLOT_2_BRAM_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_2_BRAM_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_2_BRAM_en : in STD_LOGIC;
    SLOT_2_BRAM_we : in STD_LOGIC;
    SLOT_3_BRAM_addr : in STD_LOGIC_VECTOR ( 10 downto 0 );
    SLOT_3_BRAM_din : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_3_BRAM_dout : in STD_LOGIC_VECTOR ( 31 downto 0 );
    SLOT_3_BRAM_en : in STD_LOGIC;
    SLOT_3_BRAM_we : in STD_LOGIC;
    clk : in STD_LOGIC;
    probe0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    resetn : in STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of bd_36cd : entity is "bd_36cd,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=bd_36cd,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=2,numReposBlks=2,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=SBD,synth_mode=Global}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of bd_36cd : entity is "design_1_system_ila_0_1.hwdef";
end bd_36cd;

architecture STRUCTURE of bd_36cd is
  component bd_36cd_ila_lib_0 is
  port (
    clk : in STD_LOGIC;
    probe0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe1 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    probe3 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    probe4 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    probe5 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe6 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe7 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe8 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    probe9 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    probe10 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe11 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe12 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe13 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    probe14 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe15 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    probe16 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe17 : in STD_LOGIC_VECTOR ( 10 downto 0 );
    probe18 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    probe19 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe20 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    probe21 : in STD_LOGIC_VECTOR ( 0 to 0 );
    probe22 : in STD_LOGIC_VECTOR ( 10 downto 0 )
  );
  end component bd_36cd_ila_lib_0;
  component bd_36cd_g_inst_0 is
  port (
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC;
    slot_0_axis_tvalid : in STD_LOGIC;
    slot_0_axis_tready : in STD_LOGIC;
    slot_0_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    slot_0_axis_tstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    slot_0_axis_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    slot_0_axis_tlast : in STD_LOGIC;
    slot_1_axis_tvalid : in STD_LOGIC;
    slot_1_axis_tready : in STD_LOGIC;
    slot_1_axis_tdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    slot_1_axis_tkeep : in STD_LOGIC_VECTOR ( 3 downto 0 );
    slot_1_axis_tlast : in STD_LOGIC;
    m_slot_0_axis_tvalid : out STD_LOGIC;
    m_slot_0_axis_tready : out STD_LOGIC;
    m_slot_0_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_slot_0_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_slot_0_axis_tkeep : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_slot_0_axis_tlast : out STD_LOGIC;
    m_slot_1_axis_tvalid : out STD_LOGIC;
    m_slot_1_axis_tready : out STD_LOGIC;
    m_slot_1_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_slot_1_axis_tkeep : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_slot_1_axis_tlast : out STD_LOGIC
  );
  end component bd_36cd_g_inst_0;
  signal Conn1_TDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Conn1_TKEEP : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Conn1_TLAST : STD_LOGIC;
  signal Conn1_TREADY : STD_LOGIC;
  signal Conn1_TVALID : STD_LOGIC;
  signal Conn_TDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal Conn_TKEEP : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Conn_TLAST : STD_LOGIC;
  signal Conn_TREADY : STD_LOGIC;
  signal Conn_TSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal Conn_TVALID : STD_LOGIC;
  signal SLOT_2_BRAM_addr_1 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal SLOT_2_BRAM_din_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal SLOT_2_BRAM_dout_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal SLOT_2_BRAM_en_1 : STD_LOGIC;
  signal SLOT_2_BRAM_we_1 : STD_LOGIC;
  signal SLOT_3_BRAM_addr_1 : STD_LOGIC_VECTOR ( 10 downto 0 );
  signal SLOT_3_BRAM_din_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal SLOT_3_BRAM_dout_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal SLOT_3_BRAM_en_1 : STD_LOGIC;
  signal SLOT_3_BRAM_we_1 : STD_LOGIC;
  signal clk_1 : STD_LOGIC;
  signal net_slot_0_axis_tdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal net_slot_0_axis_tkeep : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal net_slot_0_axis_tlast : STD_LOGIC;
  signal net_slot_0_axis_tready : STD_LOGIC;
  signal net_slot_0_axis_tstrb : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal net_slot_0_axis_tvalid : STD_LOGIC;
  signal net_slot_1_axis_tdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal net_slot_1_axis_tkeep : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal net_slot_1_axis_tlast : STD_LOGIC;
  signal net_slot_1_axis_tready : STD_LOGIC;
  signal net_slot_1_axis_tvalid : STD_LOGIC;
  signal probe0_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal probe1_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal resetn_1 : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of SLOT_0_AXIS_tlast : signal is "xilinx.com:interface:axis:1.0 SLOT_0_AXIS TLAST";
  attribute X_INTERFACE_INFO of SLOT_0_AXIS_tready : signal is "xilinx.com:interface:axis:1.0 SLOT_0_AXIS TREADY";
  attribute X_INTERFACE_INFO of SLOT_0_AXIS_tvalid : signal is "xilinx.com:interface:axis:1.0 SLOT_0_AXIS TVALID";
  attribute X_INTERFACE_INFO of SLOT_1_AXIS_tlast : signal is "xilinx.com:interface:axis:1.0 SLOT_1_AXIS TLAST";
  attribute X_INTERFACE_INFO of SLOT_1_AXIS_tready : signal is "xilinx.com:interface:axis:1.0 SLOT_1_AXIS TREADY";
  attribute X_INTERFACE_INFO of SLOT_1_AXIS_tvalid : signal is "xilinx.com:interface:axis:1.0 SLOT_1_AXIS TVALID";
  attribute X_INTERFACE_INFO of SLOT_2_BRAM_en : signal is "xilinx.com:interface:bram:1.0 SLOT_2_BRAM EN";
  attribute X_INTERFACE_INFO of SLOT_2_BRAM_we : signal is "xilinx.com:interface:bram:1.0 SLOT_2_BRAM WE";
  attribute X_INTERFACE_INFO of SLOT_3_BRAM_en : signal is "xilinx.com:interface:bram:1.0 SLOT_3_BRAM EN";
  attribute X_INTERFACE_INFO of SLOT_3_BRAM_we : signal is "xilinx.com:interface:bram:1.0 SLOT_3_BRAM WE";
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 CLK.CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME CLK.CLK, ASSOCIATED_BUSIF SLOT_0_AXIS:SLOT_1_AXIS, ASSOCIATED_RESET resetn, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of resetn : signal is "xilinx.com:signal:reset:1.0 RST.RESETN RST";
  attribute X_INTERFACE_PARAMETER of resetn : signal is "XIL_INTERFACENAME RST.RESETN, INSERT_VIP 0, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of SLOT_0_AXIS_tdata : signal is "xilinx.com:interface:axis:1.0 SLOT_0_AXIS TDATA";
  attribute X_INTERFACE_PARAMETER of SLOT_0_AXIS_tdata : signal is "XIL_INTERFACENAME SLOT_0_AXIS, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, FREQ_HZ 100000000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 1, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.000, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of SLOT_0_AXIS_tkeep : signal is "xilinx.com:interface:axis:1.0 SLOT_0_AXIS TKEEP";
  attribute X_INTERFACE_INFO of SLOT_0_AXIS_tstrb : signal is "xilinx.com:interface:axis:1.0 SLOT_0_AXIS TSTRB";
  attribute X_INTERFACE_INFO of SLOT_1_AXIS_tdata : signal is "xilinx.com:interface:axis:1.0 SLOT_1_AXIS TDATA";
  attribute X_INTERFACE_PARAMETER of SLOT_1_AXIS_tdata : signal is "XIL_INTERFACENAME SLOT_1_AXIS, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, FREQ_HZ 100000000, HAS_TKEEP 1, HAS_TLAST 1, HAS_TREADY 1, HAS_TSTRB 0, INSERT_VIP 0, LAYERED_METADATA undef, PHASE 0.000, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of SLOT_1_AXIS_tkeep : signal is "xilinx.com:interface:axis:1.0 SLOT_1_AXIS TKEEP";
  attribute X_INTERFACE_INFO of SLOT_2_BRAM_addr : signal is "xilinx.com:interface:bram:1.0 SLOT_2_BRAM ADDR";
  attribute X_INTERFACE_PARAMETER of SLOT_2_BRAM_addr : signal is "XIL_INTERFACENAME SLOT_2_BRAM, MASTER_TYPE BRAM_CTRL, MEM_ECC NONE, MEM_SIZE 8192, MEM_WIDTH 32, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of SLOT_2_BRAM_din : signal is "xilinx.com:interface:bram:1.0 SLOT_2_BRAM DIN";
  attribute X_INTERFACE_INFO of SLOT_2_BRAM_dout : signal is "xilinx.com:interface:bram:1.0 SLOT_2_BRAM DOUT";
  attribute X_INTERFACE_INFO of SLOT_3_BRAM_addr : signal is "xilinx.com:interface:bram:1.0 SLOT_3_BRAM ADDR";
  attribute X_INTERFACE_PARAMETER of SLOT_3_BRAM_addr : signal is "XIL_INTERFACENAME SLOT_3_BRAM, MASTER_TYPE BRAM_CTRL, MEM_ECC NONE, MEM_SIZE 8192, MEM_WIDTH 32, READ_LATENCY 1";
  attribute X_INTERFACE_INFO of SLOT_3_BRAM_din : signal is "xilinx.com:interface:bram:1.0 SLOT_3_BRAM DIN";
  attribute X_INTERFACE_INFO of SLOT_3_BRAM_dout : signal is "xilinx.com:interface:bram:1.0 SLOT_3_BRAM DOUT";
begin
  Conn1_TDATA(31 downto 0) <= SLOT_1_AXIS_tdata(31 downto 0);
  Conn1_TKEEP(3 downto 0) <= SLOT_1_AXIS_tkeep(3 downto 0);
  Conn1_TLAST <= SLOT_1_AXIS_tlast;
  Conn1_TREADY <= SLOT_1_AXIS_tready;
  Conn1_TVALID <= SLOT_1_AXIS_tvalid;
  Conn_TDATA(31 downto 0) <= SLOT_0_AXIS_tdata(31 downto 0);
  Conn_TKEEP(3 downto 0) <= SLOT_0_AXIS_tkeep(3 downto 0);
  Conn_TLAST <= SLOT_0_AXIS_tlast;
  Conn_TREADY <= SLOT_0_AXIS_tready;
  Conn_TSTRB(3 downto 0) <= SLOT_0_AXIS_tstrb(3 downto 0);
  Conn_TVALID <= SLOT_0_AXIS_tvalid;
  SLOT_2_BRAM_addr_1(10 downto 0) <= SLOT_2_BRAM_addr(10 downto 0);
  SLOT_2_BRAM_din_1(31 downto 0) <= SLOT_2_BRAM_din(31 downto 0);
  SLOT_2_BRAM_dout_1(31 downto 0) <= SLOT_2_BRAM_dout(31 downto 0);
  SLOT_2_BRAM_en_1 <= SLOT_2_BRAM_en;
  SLOT_2_BRAM_we_1 <= SLOT_2_BRAM_we;
  SLOT_3_BRAM_addr_1(10 downto 0) <= SLOT_3_BRAM_addr(10 downto 0);
  SLOT_3_BRAM_din_1(31 downto 0) <= SLOT_3_BRAM_din(31 downto 0);
  SLOT_3_BRAM_dout_1(31 downto 0) <= SLOT_3_BRAM_dout(31 downto 0);
  SLOT_3_BRAM_en_1 <= SLOT_3_BRAM_en;
  SLOT_3_BRAM_we_1 <= SLOT_3_BRAM_we;
  clk_1 <= clk;
  probe0_1(0) <= probe0(0);
  probe1_1(0) <= probe1(0);
  resetn_1 <= resetn;
g_inst: component bd_36cd_g_inst_0
     port map (
      aclk => clk_1,
      aresetn => resetn_1,
      m_slot_0_axis_tdata(31 downto 0) => net_slot_0_axis_tdata(31 downto 0),
      m_slot_0_axis_tkeep(3 downto 0) => net_slot_0_axis_tkeep(3 downto 0),
      m_slot_0_axis_tlast => net_slot_0_axis_tlast,
      m_slot_0_axis_tready => net_slot_0_axis_tready,
      m_slot_0_axis_tstrb(3 downto 0) => net_slot_0_axis_tstrb(3 downto 0),
      m_slot_0_axis_tvalid => net_slot_0_axis_tvalid,
      m_slot_1_axis_tdata(31 downto 0) => net_slot_1_axis_tdata(31 downto 0),
      m_slot_1_axis_tkeep(3 downto 0) => net_slot_1_axis_tkeep(3 downto 0),
      m_slot_1_axis_tlast => net_slot_1_axis_tlast,
      m_slot_1_axis_tready => net_slot_1_axis_tready,
      m_slot_1_axis_tvalid => net_slot_1_axis_tvalid,
      slot_0_axis_tdata(31 downto 0) => Conn_TDATA(31 downto 0),
      slot_0_axis_tkeep(3 downto 0) => Conn_TKEEP(3 downto 0),
      slot_0_axis_tlast => Conn_TLAST,
      slot_0_axis_tready => Conn_TREADY,
      slot_0_axis_tstrb(3 downto 0) => Conn_TSTRB(3 downto 0),
      slot_0_axis_tvalid => Conn_TVALID,
      slot_1_axis_tdata(31 downto 0) => Conn1_TDATA(31 downto 0),
      slot_1_axis_tkeep(3 downto 0) => Conn1_TKEEP(3 downto 0),
      slot_1_axis_tlast => Conn1_TLAST,
      slot_1_axis_tready => Conn1_TREADY,
      slot_1_axis_tvalid => Conn1_TVALID
    );
ila_lib: component bd_36cd_ila_lib_0
     port map (
      clk => clk_1,
      probe0(0) => probe0_1(0),
      probe1(0) => probe1_1(0),
      probe10(0) => net_slot_1_axis_tvalid,
      probe11(0) => net_slot_1_axis_tready,
      probe12(0) => net_slot_1_axis_tlast,
      probe13(31 downto 0) => SLOT_2_BRAM_din_1(31 downto 0),
      probe14(0) => SLOT_2_BRAM_en_1,
      probe15(31 downto 0) => SLOT_2_BRAM_dout_1(31 downto 0),
      probe16(0) => SLOT_2_BRAM_we_1,
      probe17(10 downto 0) => SLOT_2_BRAM_addr_1(10 downto 0),
      probe18(31 downto 0) => SLOT_3_BRAM_din_1(31 downto 0),
      probe19(0) => SLOT_3_BRAM_en_1,
      probe2(31 downto 0) => net_slot_0_axis_tdata(31 downto 0),
      probe20(31 downto 0) => SLOT_3_BRAM_dout_1(31 downto 0),
      probe21(0) => SLOT_3_BRAM_we_1,
      probe22(10 downto 0) => SLOT_3_BRAM_addr_1(10 downto 0),
      probe3(3 downto 0) => net_slot_0_axis_tkeep(3 downto 0),
      probe4(3 downto 0) => net_slot_0_axis_tstrb(3 downto 0),
      probe5(0) => net_slot_0_axis_tvalid,
      probe6(0) => net_slot_0_axis_tready,
      probe7(0) => net_slot_0_axis_tlast,
      probe8(31 downto 0) => net_slot_1_axis_tdata(31 downto 0),
      probe9(3 downto 0) => net_slot_1_axis_tkeep(3 downto 0)
    );
end STRUCTURE;
