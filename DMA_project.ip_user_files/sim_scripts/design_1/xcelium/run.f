-makelib xcelium_lib/xilinx_vip -sv \
  "C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
  "C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
  "C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
  "C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
  "C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
  "C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
  "C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
  "C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
  "C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/rst_vip_if.sv" \
-endlib
-makelib xcelium_lib/xil_defaultlib -sv \
  "C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
  "C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
  "C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \
-endlib
-makelib xcelium_lib/xpm \
  "C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \
-endlib
-makelib xcelium_lib/axi_infrastructure_v1_1_0 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_vip_v1_1_5 -sv \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/d4a8/hdl/axi_vip_v1_1_vl_rfs.sv" \
-endlib
-makelib xcelium_lib/processing_system7_vip_v1_0_7 -sv \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl/processing_system7_vip_v1_0_vl_rfs.sv" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_processing_system7_0_0/sim/design_1_processing_system7_0_0.v" \
-endlib
-makelib xcelium_lib/lib_pkg_v1_0_2 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/0513/hdl/lib_pkg_v1_0_rfs.vhd" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_4 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1f5a/simulation/fifo_generator_vlog_beh.v" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_4 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.vhd" \
-endlib
-makelib xcelium_lib/fifo_generator_v13_2_4 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.v" \
-endlib
-makelib xcelium_lib/lib_fifo_v1_0_13 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/4dac/hdl/lib_fifo_v1_0_rfs.vhd" \
-endlib
-makelib xcelium_lib/lib_srl_fifo_v1_0_2 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/51ce/hdl/lib_srl_fifo_v1_0_rfs.vhd" \
-endlib
-makelib xcelium_lib/lib_cdc_v1_0_2 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \
-endlib
-makelib xcelium_lib/axi_datamover_v5_1_21 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/e644/hdl/axi_datamover_v5_1_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/axi_sg_v4_1_12 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/91f3/hdl/axi_sg_v4_1_rfs.vhd" \
-endlib
-makelib xcelium_lib/axi_dma_v7_1_20 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/260a/hdl/axi_dma_v7_1_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_axi_dma_0_0/sim/design_1_axi_dma_0_0.vhd" \
-endlib
-makelib xcelium_lib/proc_sys_reset_v5_0_13 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8842/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_rst_ps7_0_100M_0/sim/design_1_rst_ps7_0_100M_0.vhd" \
-endlib
-makelib xcelium_lib/generic_baseblocks_v2_1_0 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_register_slice_v2_1_19 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/4d88/hdl/axi_register_slice_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_data_fifo_v2_1_18 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/5b9c/hdl/axi_data_fifo_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/axi_crossbar_v2_1_20 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ace7/hdl/axi_crossbar_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_xbar_0/sim/design_1_xbar_0.v" \
-endlib
-makelib xcelium_lib/xlconcat_v2_1_3 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/442e/hdl/xlconcat_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_xlconcat_0_0/sim/design_1_xlconcat_0_0.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_system_ila_0_1/bd_0/ip/ip_0/sim/bd_36cd_ila_lib_0.vhd" \
-endlib
-makelib xcelium_lib/gigantic_mux \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/d322/hdl/gigantic_mux_v1_0_cntr.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_system_ila_0_1/bd_0/ip/ip_1/bd_36cd_g_inst_0_gigantic_mux.v" \
  "../../../bd/design_1/ip/design_1_system_ila_0_1/bd_0/ip/ip_1/sim/bd_36cd_g_inst_0.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_system_ila_0_1/bd_0/sim/bd_36cd.vhd" \
  "../../../bd/design_1/ip/design_1_system_ila_0_1/sim/design_1_system_ila_0_1.vhd" \
  "../../../bd/design_1/ipshared/33bc/hdl/brams_core.vhd" \
  "../../../bd/design_1/ipshared/33bc/src/true_dual_bram.vhd" \
  "../../../bd/design_1/ipshared/33bc/hdl/dual_bram_axis_v1_0.vhd" \
  "../../../bd/design_1/ip/design_1_dual_bram_axis_0_0/sim/design_1_dual_bram_axis_0_0.vhd" \
-endlib
-makelib xcelium_lib/axi_lite_ipif_v3_0_4 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/66ea/hdl/axi_lite_ipif_v3_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/interrupt_control_v3_1_4 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/a040/hdl/interrupt_control_v3_1_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/axi_gpio_v2_0_21 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9c6e/hdl/axi_gpio_v2_0_vh_rfs.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_axi_gpio_0_0/sim/design_1_axi_gpio_0_0.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_xbar_1/sim/design_1_xbar_1.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ipshared/23a6/hdl/timer2_v1_0.vhd" \
  "../../../bd/design_1/ip/design_1_timer2_0_0/sim/design_1_timer2_0_0.vhd" \
  "../../../bd/design_1/ip/design_1_axi_gpio_1_0/sim/design_1_axi_gpio_1_0.vhd" \
  "../../../bd/design_1/ip/design_1_axi_gpio_2_0/sim/design_1_axi_gpio_2_0.vhd" \
  "../../../bd/design_1/ipshared/53ad/hdl/poly_tomont_v1_0.vhd" \
  "../../../bd/design_1/ip/design_1_poly_tomont_0_0/sim/design_1_poly_tomont_0_0.vhd" \
  "../../../bd/design_1/ipshared/ab6a/hdl/bram_port_selector_v1_0.vhd" \
  "../../../bd/design_1/ip/design_1_bram_port_selector_0_0/sim/design_1_bram_port_selector_0_0.vhd" \
  "../../../bd/design_1/ip/design_1_bram_port_selector_1_0/sim/design_1_bram_port_selector_1_0.vhd" \
  "../../../bd/design_1/ipshared/9555/hdl/signal_multiplexer_v1_0.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_0_0/sim/design_1_signal_multiplexer_0_0.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_0_1/sim/design_1_signal_multiplexer_0_1.vhd" \
  "../../../bd/design_1/ipshared/6b7a/hdl/montgomery_reduction_v1_0.vhd" \
  "../../../bd/design_1/ip/design_1_montgomery_reduction_0_0/sim/design_1_montgomery_reduction_0_0.vhd" \
  "../../../bd/design_1/ip/design_1_montgomery_reduction_0_1/sim/design_1_montgomery_reduction_0_1.vhd" \
  "../../../bd/design_1/ipshared/b618/src/polyvec_basemul_acc_montgomery_v1_0.vhd" \
  "../../../bd/design_1/ip/design_1_polyvec_basemul_acc_0_0/sim/design_1_polyvec_basemul_acc_0_0.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_2_0/sim/design_1_signal_multiplexer_2_0.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_2_1/sim/design_1_signal_multiplexer_2_1.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_3_0/sim/design_1_signal_multiplexer_3_0.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_3_1/sim/design_1_signal_multiplexer_3_1.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_3_2/sim/design_1_signal_multiplexer_3_2.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_3_3/sim/design_1_signal_multiplexer_3_3.vhd" \
  "../../../bd/design_1/ip/design_1_bram_port_selector_2_0/sim/design_1_bram_port_selector_2_0.vhd" \
  "../../../bd/design_1/ip/design_1_bram_port_selector_3_0/sim/design_1_bram_port_selector_3_0.vhd" \
  "../../../bd/design_1/ipshared/71a8/hdl/fqmul_v1_0.vhd" \
  "../../../bd/design_1/ip/design_1_fqmul_0_0/sim/design_1_fqmul_0_0.vhd" \
  "../../../bd/design_1/ip/design_1_fqmul_0_1/sim/design_1_fqmul_0_1.vhd" \
  "../../../bd/design_1/ip/design_1_fqmul_0_2/sim/design_1_fqmul_0_2.vhd" \
  "../../../bd/design_1/ip/design_1_fqmul_0_3/sim/design_1_fqmul_0_3.vhd" \
  "../../../bd/design_1/ip/design_1_fqmul_0_4/sim/design_1_fqmul_0_4.vhd" \
  "../../../bd/design_1/ip/design_1_fqmul_0_5/sim/design_1_fqmul_0_5.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_1_0/sim/design_1_signal_multiplexer_1_0.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_1_1/sim/design_1_signal_multiplexer_1_1.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_1_2/sim/design_1_signal_multiplexer_1_2.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_1_3/sim/design_1_signal_multiplexer_1_3.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_1_4/sim/design_1_signal_multiplexer_1_4.vhd" \
  "../../../bd/design_1/ip/design_1_signal_multiplexer_1_5/sim/design_1_signal_multiplexer_1_5.vhd" \
  "../../../bd/design_1/ip/design_1_montgomery_reduction_1_0/sim/design_1_montgomery_reduction_1_0.vhd" \
  "../../../bd/design_1/ip/design_1_montgomery_reduction_1_1/sim/design_1_montgomery_reduction_1_1.vhd" \
  "../../../bd/design_1/ip/design_1_montgomery_reduction_1_2/sim/design_1_montgomery_reduction_1_2.vhd" \
  "../../../bd/design_1/ip/design_1_montgomery_reduction_1_3/sim/design_1_montgomery_reduction_1_3.vhd" \
  "../../../bd/design_1/ipshared/56a9/hdl/barrett_reduce_v1_0.vhd" \
  "../../../bd/design_1/ip/design_1_barrett_reduce_0_0/sim/design_1_barrett_reduce_0_0.vhd" \
  "../../../bd/design_1/ip/design_1_barrett_reduce_1_0/sim/design_1_barrett_reduce_1_0.vhd" \
  "../../../bd/design_1/ip/design_1_axi_gpio_3_0/sim/design_1_axi_gpio_3_0.vhd" \
  "../../../bd/design_1/ip/design_1_axi_gpio_4_0/sim/design_1_axi_gpio_4_0.vhd" \
-endlib
-makelib xcelium_lib/axi_protocol_converter_v2_1_19 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/c83a/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_auto_pc_0/sim/design_1_auto_pc_0.v" \
-endlib
-makelib xcelium_lib/axi_clock_converter_v2_1_18 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ac9d/hdl/axi_clock_converter_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/blk_mem_gen_v8_4_3 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/c001/simulation/blk_mem_gen_v8_4.v" \
-endlib
-makelib xcelium_lib/axi_dwidth_converter_v2_1_19 \
  "../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/e578/hdl/axi_dwidth_converter_v2_1_vl_rfs.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/ip/design_1_auto_us_0/sim/design_1_auto_us_0.v" \
  "../../../bd/design_1/ip/design_1_auto_us_1/sim/design_1_auto_us_1.v" \
  "../../../bd/design_1/ip/design_1_auto_pc_1/sim/design_1_auto_pc_1.v" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  "../../../bd/design_1/sim/design_1.vhd" \
-endlib
-makelib xcelium_lib/xil_defaultlib \
  glbl.v
-endlib

