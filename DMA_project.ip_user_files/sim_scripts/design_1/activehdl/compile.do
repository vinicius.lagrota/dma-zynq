vlib work
vlib activehdl

vlib activehdl/xilinx_vip
vlib activehdl/xil_defaultlib
vlib activehdl/xpm
vlib activehdl/axi_infrastructure_v1_1_0
vlib activehdl/axi_vip_v1_1_5
vlib activehdl/processing_system7_vip_v1_0_7
vlib activehdl/lib_pkg_v1_0_2
vlib activehdl/fifo_generator_v13_2_4
vlib activehdl/lib_fifo_v1_0_13
vlib activehdl/lib_srl_fifo_v1_0_2
vlib activehdl/lib_cdc_v1_0_2
vlib activehdl/axi_datamover_v5_1_21
vlib activehdl/axi_sg_v4_1_12
vlib activehdl/axi_dma_v7_1_20
vlib activehdl/proc_sys_reset_v5_0_13
vlib activehdl/generic_baseblocks_v2_1_0
vlib activehdl/axi_register_slice_v2_1_19
vlib activehdl/axi_data_fifo_v2_1_18
vlib activehdl/axi_crossbar_v2_1_20
vlib activehdl/xlconcat_v2_1_3
vlib activehdl/gigantic_mux
vlib activehdl/axi_lite_ipif_v3_0_4
vlib activehdl/interrupt_control_v3_1_4
vlib activehdl/axi_gpio_v2_0_21
vlib activehdl/axi_protocol_converter_v2_1_19
vlib activehdl/axi_clock_converter_v2_1_18
vlib activehdl/blk_mem_gen_v8_4_3
vlib activehdl/axi_dwidth_converter_v2_1_19

vmap xilinx_vip activehdl/xilinx_vip
vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm
vmap axi_infrastructure_v1_1_0 activehdl/axi_infrastructure_v1_1_0
vmap axi_vip_v1_1_5 activehdl/axi_vip_v1_1_5
vmap processing_system7_vip_v1_0_7 activehdl/processing_system7_vip_v1_0_7
vmap lib_pkg_v1_0_2 activehdl/lib_pkg_v1_0_2
vmap fifo_generator_v13_2_4 activehdl/fifo_generator_v13_2_4
vmap lib_fifo_v1_0_13 activehdl/lib_fifo_v1_0_13
vmap lib_srl_fifo_v1_0_2 activehdl/lib_srl_fifo_v1_0_2
vmap lib_cdc_v1_0_2 activehdl/lib_cdc_v1_0_2
vmap axi_datamover_v5_1_21 activehdl/axi_datamover_v5_1_21
vmap axi_sg_v4_1_12 activehdl/axi_sg_v4_1_12
vmap axi_dma_v7_1_20 activehdl/axi_dma_v7_1_20
vmap proc_sys_reset_v5_0_13 activehdl/proc_sys_reset_v5_0_13
vmap generic_baseblocks_v2_1_0 activehdl/generic_baseblocks_v2_1_0
vmap axi_register_slice_v2_1_19 activehdl/axi_register_slice_v2_1_19
vmap axi_data_fifo_v2_1_18 activehdl/axi_data_fifo_v2_1_18
vmap axi_crossbar_v2_1_20 activehdl/axi_crossbar_v2_1_20
vmap xlconcat_v2_1_3 activehdl/xlconcat_v2_1_3
vmap gigantic_mux activehdl/gigantic_mux
vmap axi_lite_ipif_v3_0_4 activehdl/axi_lite_ipif_v3_0_4
vmap interrupt_control_v3_1_4 activehdl/interrupt_control_v3_1_4
vmap axi_gpio_v2_0_21 activehdl/axi_gpio_v2_0_21
vmap axi_protocol_converter_v2_1_19 activehdl/axi_protocol_converter_v2_1_19
vmap axi_clock_converter_v2_1_18 activehdl/axi_clock_converter_v2_1_18
vmap blk_mem_gen_v8_4_3 activehdl/blk_mem_gen_v8_4_3
vmap axi_dwidth_converter_v2_1_19 activehdl/axi_dwidth_converter_v2_1_19

vlog -work xilinx_vip  -sv2k12 "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_axi4streampc.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_axi4pc.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/xil_common_vip_pkg.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_pkg.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_pkg.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi4stream_vip_if.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/axi_vip_if.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/clk_vip_if.sv" \
"C:/Xilinx/Vivado/2019.1/data/xilinx_vip/hdl/rst_vip_if.sv" \

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work axi_infrastructure_v1_1_0  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work axi_vip_v1_1_5  -sv2k12 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/d4a8/hdl/axi_vip_v1_1_vl_rfs.sv" \

vlog -work processing_system7_vip_v1_0_7  -sv2k12 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl/processing_system7_vip_v1_0_vl_rfs.sv" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_processing_system7_0_0/sim/design_1_processing_system7_0_0.v" \

vcom -work lib_pkg_v1_0_2 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/0513/hdl/lib_pkg_v1_0_rfs.vhd" \

vlog -work fifo_generator_v13_2_4  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1f5a/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_2_4 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.vhd" \

vlog -work fifo_generator_v13_2_4  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.v" \

vcom -work lib_fifo_v1_0_13 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/4dac/hdl/lib_fifo_v1_0_rfs.vhd" \

vcom -work lib_srl_fifo_v1_0_2 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/51ce/hdl/lib_srl_fifo_v1_0_rfs.vhd" \

vcom -work lib_cdc_v1_0_2 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \

vcom -work axi_datamover_v5_1_21 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/e644/hdl/axi_datamover_v5_1_vh_rfs.vhd" \

vcom -work axi_sg_v4_1_12 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/91f3/hdl/axi_sg_v4_1_rfs.vhd" \

vcom -work axi_dma_v7_1_20 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/260a/hdl/axi_dma_v7_1_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_axi_dma_0_0/sim/design_1_axi_dma_0_0.vhd" \

vcom -work proc_sys_reset_v5_0_13 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8842/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_rst_ps7_0_100M_0/sim/design_1_rst_ps7_0_100M_0.vhd" \

vlog -work generic_baseblocks_v2_1_0  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_19  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/4d88/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work axi_data_fifo_v2_1_18  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/5b9c/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_crossbar_v2_1_20  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ace7/hdl/axi_crossbar_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_xbar_0/sim/design_1_xbar_0.v" \

vlog -work xlconcat_v2_1_3  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/442e/hdl/xlconcat_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_xlconcat_0_0/sim/design_1_xlconcat_0_0.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_system_ila_0_1/bd_0/ip/ip_0/sim/bd_36cd_ila_lib_0.vhd" \

vlog -work gigantic_mux  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/d322/hdl/gigantic_mux_v1_0_cntr.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_system_ila_0_1/bd_0/ip/ip_1/bd_36cd_g_inst_0_gigantic_mux.v" \
"../../../bd/design_1/ip/design_1_system_ila_0_1/bd_0/ip/ip_1/sim/bd_36cd_g_inst_0.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_system_ila_0_1/bd_0/sim/bd_36cd.vhd" \
"../../../bd/design_1/ip/design_1_system_ila_0_1/sim/design_1_system_ila_0_1.vhd" \
"../../../bd/design_1/ipshared/33bc/hdl/brams_core.vhd" \
"../../../bd/design_1/ipshared/33bc/src/true_dual_bram.vhd" \
"../../../bd/design_1/ipshared/33bc/hdl/dual_bram_axis_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_dual_bram_axis_0_0/sim/design_1_dual_bram_axis_0_0.vhd" \

vcom -work axi_lite_ipif_v3_0_4 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/66ea/hdl/axi_lite_ipif_v3_0_vh_rfs.vhd" \

vcom -work interrupt_control_v3_1_4 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/a040/hdl/interrupt_control_v3_1_vh_rfs.vhd" \

vcom -work axi_gpio_v2_0_21 -93 \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9c6e/hdl/axi_gpio_v2_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ip/design_1_axi_gpio_0_0/sim/design_1_axi_gpio_0_0.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_xbar_1/sim/design_1_xbar_1.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/ipshared/23a6/hdl/timer2_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_timer2_0_0/sim/design_1_timer2_0_0.vhd" \
"../../../bd/design_1/ip/design_1_axi_gpio_1_0/sim/design_1_axi_gpio_1_0.vhd" \
"../../../bd/design_1/ip/design_1_axi_gpio_2_0/sim/design_1_axi_gpio_2_0.vhd" \
"../../../bd/design_1/ipshared/53ad/hdl/poly_tomont_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_poly_tomont_0_0/sim/design_1_poly_tomont_0_0.vhd" \
"../../../bd/design_1/ipshared/ab6a/hdl/bram_port_selector_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_bram_port_selector_0_0/sim/design_1_bram_port_selector_0_0.vhd" \
"../../../bd/design_1/ip/design_1_bram_port_selector_1_0/sim/design_1_bram_port_selector_1_0.vhd" \
"../../../bd/design_1/ipshared/9555/hdl/signal_multiplexer_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_0_0/sim/design_1_signal_multiplexer_0_0.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_0_1/sim/design_1_signal_multiplexer_0_1.vhd" \
"../../../bd/design_1/ipshared/6b7a/hdl/montgomery_reduction_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_montgomery_reduction_0_0/sim/design_1_montgomery_reduction_0_0.vhd" \
"../../../bd/design_1/ip/design_1_montgomery_reduction_0_1/sim/design_1_montgomery_reduction_0_1.vhd" \
"../../../bd/design_1/ipshared/b618/src/polyvec_basemul_acc_montgomery_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_polyvec_basemul_acc_0_0/sim/design_1_polyvec_basemul_acc_0_0.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_2_0/sim/design_1_signal_multiplexer_2_0.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_2_1/sim/design_1_signal_multiplexer_2_1.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_3_0/sim/design_1_signal_multiplexer_3_0.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_3_1/sim/design_1_signal_multiplexer_3_1.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_3_2/sim/design_1_signal_multiplexer_3_2.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_3_3/sim/design_1_signal_multiplexer_3_3.vhd" \
"../../../bd/design_1/ip/design_1_bram_port_selector_2_0/sim/design_1_bram_port_selector_2_0.vhd" \
"../../../bd/design_1/ip/design_1_bram_port_selector_3_0/sim/design_1_bram_port_selector_3_0.vhd" \
"../../../bd/design_1/ipshared/71a8/hdl/fqmul_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_fqmul_0_0/sim/design_1_fqmul_0_0.vhd" \
"../../../bd/design_1/ip/design_1_fqmul_0_1/sim/design_1_fqmul_0_1.vhd" \
"../../../bd/design_1/ip/design_1_fqmul_0_2/sim/design_1_fqmul_0_2.vhd" \
"../../../bd/design_1/ip/design_1_fqmul_0_3/sim/design_1_fqmul_0_3.vhd" \
"../../../bd/design_1/ip/design_1_fqmul_0_4/sim/design_1_fqmul_0_4.vhd" \
"../../../bd/design_1/ip/design_1_fqmul_0_5/sim/design_1_fqmul_0_5.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_1_0/sim/design_1_signal_multiplexer_1_0.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_1_1/sim/design_1_signal_multiplexer_1_1.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_1_2/sim/design_1_signal_multiplexer_1_2.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_1_3/sim/design_1_signal_multiplexer_1_3.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_1_4/sim/design_1_signal_multiplexer_1_4.vhd" \
"../../../bd/design_1/ip/design_1_signal_multiplexer_1_5/sim/design_1_signal_multiplexer_1_5.vhd" \
"../../../bd/design_1/ip/design_1_montgomery_reduction_1_0/sim/design_1_montgomery_reduction_1_0.vhd" \
"../../../bd/design_1/ip/design_1_montgomery_reduction_1_1/sim/design_1_montgomery_reduction_1_1.vhd" \
"../../../bd/design_1/ip/design_1_montgomery_reduction_1_2/sim/design_1_montgomery_reduction_1_2.vhd" \
"../../../bd/design_1/ip/design_1_montgomery_reduction_1_3/sim/design_1_montgomery_reduction_1_3.vhd" \
"../../../bd/design_1/ipshared/56a9/hdl/barrett_reduce_v1_0.vhd" \
"../../../bd/design_1/ip/design_1_barrett_reduce_0_0/sim/design_1_barrett_reduce_0_0.vhd" \
"../../../bd/design_1/ip/design_1_barrett_reduce_1_0/sim/design_1_barrett_reduce_1_0.vhd" \
"../../../bd/design_1/ip/design_1_axi_gpio_3_0/sim/design_1_axi_gpio_3_0.vhd" \
"../../../bd/design_1/ip/design_1_axi_gpio_4_0/sim/design_1_axi_gpio_4_0.vhd" \

vlog -work axi_protocol_converter_v2_1_19  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/c83a/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_auto_pc_0/sim/design_1_auto_pc_0.v" \

vlog -work axi_clock_converter_v2_1_18  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ac9d/hdl/axi_clock_converter_v2_1_vl_rfs.v" \

vlog -work blk_mem_gen_v8_4_3  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/c001/simulation/blk_mem_gen_v8_4.v" \

vlog -work axi_dwidth_converter_v2_1_19  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/e578/hdl/axi_dwidth_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/ec67/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/8c62/hdl" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/1b7e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/122e/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/6887/hdl/verilog" "+incdir+../../../../DMA_project.srcs/sources_1/bd/design_1/ipshared/9623/hdl/verilog" "+incdir+C:/Xilinx/Vivado/2019.1/data/xilinx_vip/include" \
"../../../bd/design_1/ip/design_1_auto_us_0/sim/design_1_auto_us_0.v" \
"../../../bd/design_1/ip/design_1_auto_us_1/sim/design_1_auto_us_1.v" \
"../../../bd/design_1/ip/design_1_auto_pc_1/sim/design_1_auto_pc_1.v" \

vcom -work xil_defaultlib -93 \
"../../../bd/design_1/sim/design_1.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

