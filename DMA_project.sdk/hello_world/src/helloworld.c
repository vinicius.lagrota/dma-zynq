/******************************************************************************
*
* Copyright (C) 2010 - 2019 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/
/*****************************************************************************/
/**
 *
 * @file xaxidma_example_sg_poll.c
 *
 * This file demonstrates how to use the xaxidma driver on the Xilinx AXI
 * DMA core (AXIDMA) to transfer packets in polling mode when the AXIDMA
 * core is configured in Scatter Gather Mode.
 *
 * This code assumes a loopback hardware widget is connected to the AXI DMA
 * core for data packet loopback.
 *
 * To see the debug print, you need a Uart16550 or uartlite in your system,
 * and please set "-DDEBUG" in your compiler options. You need to rebuild your
 * software executable.
 *
 * Make sure that MEMORY_BASE is defined properly as per the HW system. The
 * h/w system built in Area mode has a maximum DDR memory limit of 64MB. In
 * throughput mode, it is 512MB.  These limits are need to ensured for
 * proper operation of this code.
 *
 *
 * <pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -------------------------------------------------------
 * 1.00a jz   05/17/10 First release
 * 2.00a jz   08/10/10 Second release, added in xaxidma_g.c, xaxidma_sinit.c,
 *                     updated tcl file, added xaxidma_porting_guide.h, removed
 *                     workaround for endianness
 * 4.00a rkv  02/22/11 Name of the file has been changed for naming consistency
 *       	       	   Added interrupt support for ARM.
 * 5.00a srt  03/05/12 Added Flushing and Invalidation of Caches to fix CRs
 *		       		   648103, 648701.
 *		       		   Added V7 DDR Base Address to fix CR 649405.
 * 6.00a srt  03/27/12 Changed API calls to support MCDMA driver.
 * 7.00a srt  06/18/12 API calls are reverted back for backward compatibility.
 * 7.01a srt  11/02/12 Buffer sizes (Tx and Rx) are modified to meet maximum
 *		       DDR memory limit of the h/w system built with Area mode
 * 7.02a srt  03/01/13 Updated DDR base address for IPI designs (CR 703656).
 * 9.1   adk  01/07/16 Updated DDR base address for Ultrascale (CR 799532) and
 *		       removed the defines for S6/V6.
 * 9.2   vak  15/04/16 Fixed compilation warnings in th example
 * 9.3   ms   01/23/17 Modified xil_printf statement in main function to
 *                     ensure that "Successfully ran" and "Failed" strings are
 *                     available in all examples. This is a fix for CR-965028.
 * 9.9   rsp  01/21/19 Fix use of #elif check in deriving DDR_BASE_ADDR.
 * </pre>
 *
 * ***************************************************************************
 */
/***************************** Include Files *********************************/

#include "xparameters.h"
#include "xdebug.h"
#include "sleep.h"
#include "xgpio.h"
#include "include/global_def.h"
#include "include/poly.h"
#include "include/polyvec.h"
#include "include/fips202.h"

//////////////////////////////////////////////
//
//	DEFINES TESTS
//
//////////////////////////////////////////////
#define TEST_POLY_TOMONT				0
#define TEST_POLYVEC_ACC				1

#ifdef __aarch64__
#include "xil_mmu.h"
#endif

#if defined(XPAR_UARTNS550_0_BASEADDR)
#include "xuartns550_l.h"       /* to use uartns550 */
#endif

#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif


/**************************** Type Definitions *******************************/

/************************** Function Prototypes ******************************/
#if defined(XPAR_UARTNS550_0_BASEADDR)
static void Uart550_Setup(void);
#endif

extern XGpio_Config * XGpioConfigPtrGlobalTimer;
extern XGpio XGpioGlobalTimer;

extern XGpio_Config * XGpioConfigTomontAndReduce;
extern XGpio XGpioTomontAndReduce;

extern XGpio_Config * XGpioConfigAccMontKeccak;
extern XGpio XGpioAccMontKeccak;

extern XGpio_Config * XGpioConfigDma;
extern XGpio XGpioDma;

extern XAxiDma_Config * XAxiDmaConfig;
extern XAxiDma XAxiDmaPtr;

extern u8 *TxBufferPtr;
extern u8 *RxBufferPtr;

/************************** Variable Definitions *****************************/
/*
 * Device instance definitions
 */

/*****************************************************************************/
/**
*
* Main function
*
* This function is the main entry of the tests on DMA core. It sets up
* DMA engine to be ready to receive and send packets, then a packet is
* transmitted and will be verified after it is received via the DMA loopback
* widget.
*
* @param	None
*
* @return
*		- XST_SUCCESS if test passes
*		- XST_FAILURE if test fails.
*
* @note		None.
*
******************************************************************************/
int main(void)
{
	int Status;

	TxBufferPtr = (u8 *)TX_BUFFER_BASE ;
	RxBufferPtr = (u8 *)RX_BUFFER_BASE;

#if defined(XPAR_UARTNS550_0_BASEADDR)

	Uart550_Setup();

#endif

	xil_printf("\r\n--- Entering main() --- \r\n");

#ifdef __aarch64__
	Xil_SetTlbAttributes(TX_BD_SPACE_BASE, MARK_UNCACHEABLE);
	Xil_SetTlbAttributes(RX_BD_SPACE_BASE, MARK_UNCACHEABLE);
#endif
	XGpioConfigDma = XGpio_LookupConfig(XPAR_AXI_GPIO_0_BASEADDR);
	XGpio_CfgInitialize(&XGpioDma, XGpioConfigDma, XGpioConfigDma->BaseAddress);

	//---- Configure timers ----
	configTimer(XGpioConfigPtrGlobalTimer, &XGpioGlobalTimer, XPAR_AXI_GPIO_1_DEVICE_ID, 1);

	//Poly tomont and reduce
	XGpioConfigTomontAndReduce = XGpio_LookupConfig(XPAR_AXI_GPIO_2_DEVICE_ID);
	XGpio_CfgInitialize(&XGpioTomontAndReduce, XGpioConfigTomontAndReduce, XGpioConfigTomontAndReduce->BaseAddress);

	//Polyvec basemul acc montgomery
	XGpioConfigAccMontKeccak = XGpio_LookupConfig(XPAR_AXI_GPIO_3_DEVICE_ID);
	XGpio_CfgInitialize(&XGpioAccMontKeccak, XGpioConfigAccMontKeccak, XGpioConfigAccMontKeccak->BaseAddress);

	//---- Configure Kyber K ----
	configKyberK(XGpioConfigKyberK, &XGpioKyberK, XPAR_AXI_GPIO_4_DEVICE_ID, 1);
	print_debug(DEBUG_MAIN, "[MAIN] Parameter KYBER_K: %ld.\n", XGpio_DiscreteRead(&XGpioKyberK, 1));

	//Values < 8192 (or <= 8160) and must be 32 bytes aligned (size/32 = integer).
//	size_t sTxSizePacket = 512; //bytes
//	size_t sRxSizePacket = 512; //bytes

	XAxiDmaConfig = XAxiDma_LookupConfig(XPAR_AXIDMA_0_DEVICE_ID);
	if (!XAxiDmaConfig) {
		xil_printf("No config found for %d\r\n", XPAR_AXIDMA_0_DEVICE_ID);
		return XST_FAILURE;
	}

	Status = XAxiDma_CfgInitialize(&XAxiDmaPtr, XAxiDmaConfig);
	if (Status != XST_SUCCESS) {
		xil_printf("Initialization failed %d\r\n", Status);
		return XST_FAILURE;
	}

	if(XAxiDma_HasSg(&XAxiDmaPtr)){
		xil_printf("Device configured as SG mode \r\n");
		return XST_FAILURE;
	}

	/* Disable interrupts, we use polling mode
	 */
	XAxiDma_IntrDisable(&XAxiDmaPtr, XAXIDMA_IRQ_ALL_MASK,
						XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(&XAxiDmaPtr, XAXIDMA_IRQ_ALL_MASK,
						XAXIDMA_DMA_TO_DEVICE);

	int j = 0x0;

	while(1)
	{
#if TEST_POLY_TOMONT == 1
		j++;

		//Poly tomont test
		poly r1;
		poly r2;
		poly r3 = {0};
		poly r_test = {0};
		for(int i = 0; i < 256; i = i + 4)
		{
			r_test.coeffs[i + 0] = 0x03fb + j;
			r_test.coeffs[i + 1] = 0x062e + j;
			r_test.coeffs[i + 2] = 0x0593 + j;
			r_test.coeffs[i + 3] = 0x039b + j;
		}

		for(int i = 0; i < 8; i++)
		{
			xil_printf("r_test.coeffs[%d]: %x\r\n", i, r_test.coeffs[i]);
		}

		memcpy(&r1, &r_test, 512);
		memcpy(&r2, &r_test, 512);

		resetTimer(&XGpioGlobalTimer, 1);
		u32 u32Timer1 = getTimer(&XGpioGlobalTimer, 1);
		xil_printf("[MAIN] Reset Timer HW: %ld ns\n", u32Timer1 * 10);
		startTimer(&XGpioGlobalTimer, 1);

		poly_tomont_hw(&r1);

		stopTimer(&XGpioGlobalTimer, 1);
		u32Timer1 = getTimer(&XGpioGlobalTimer, 1);

		//Received
		for(int i = 0; i < 8; i++)
		{
			xil_printf("r1.coeffs[%d]: %x\r\n", i, r1.coeffs[i]);
		}

		resetTimer(&XGpioGlobalTimer, 1);
		u32 u32Timer2 = getTimer(&XGpioGlobalTimer, 1);
		print_debug(DEBUG_MAIN, "[MAIN] Reset Timer SW: %ld ns\n", u32Timer2 * 10);
		startTimer(&XGpioGlobalTimer, 1);

		poly_tomont_sw(&r2);

		stopTimer(&XGpioGlobalTimer, 1);
		u32Timer2 = getTimer(&XGpioGlobalTimer, 1);
//		for(int i = 0; i < 4; i++)
//		{
//			print_debug(DEBUG_MAIN, "[MAIN] poly2 result[%d]: 0x%04x\n", i, r2.coeffs[i]);
//		}
		for(int i = 0; i < 8; i++)
		{
			xil_printf("r2.coeffs[%d]: %x\r\n", i, r2.coeffs[i]);
		}

		if(memcmp(&r1, &r2, 512) != 0)
		{
			print_debug(DEBUG_MAIN, "[MAIN] Error!\n");
			exit(0);
		}
		else
			print_debug(DEBUG_MAIN, "[MAIN] Ok!\n");

		print_debug(DEBUG_MAIN, "[MAIN] Timer SW: %ld ns\n", u32Timer2 * HW_CLOCK_PERIOD);
		print_debug(DEBUG_MAIN, "[MAIN] Timer HW: %ld ns\n", u32Timer1 * HW_CLOCK_PERIOD);
#endif

#if TEST_POLYVEC_ACC == 1
		//Test polyvec basemul acc montgomery
		poly r;
		poly r3;
		polyvec r1, r2;

		for (int i = 0; i < 256; i += 2)
		{
			r1.vec[0].coeffs[i] = i + 1;
			r1.vec[0].coeffs[i + 1] = i + 2;
		}
		for (int i = 0; i < 256; i += 2)
		{
			r1.vec[1].coeffs[i] = i + 0x0801;
			r1.vec[1].coeffs[i + 1] = i + 0x0802;
		}
		for (int i = 0; i < 256; i += 2)
		{
			r2.vec[0].coeffs[i] = i + 0xF000;
			r2.vec[0].coeffs[i + 1] = i + 0xF001;
		}
		for (int i = 0; i < 256; i += 2)
		{
			r2.vec[1].coeffs[i] = i + 0xF800;
			r2.vec[1].coeffs[i + 1] = i + 0xF801;
		}

		if(u32SystemState & POLYVEC_BASEMUL_MASK)
			print_debug(DEBUG_MAIN, "[POLYVEC] polyvec_basemul_acc_montgomery_hw\n");
		else
			print_debug(DEBUG_MAIN, "[POLYVEC] polyvec_basemul_acc_montgomery_sw\n");

		resetTimer(&XGpioGlobalTimer, 1);
		u32 u32Timer5 = getTimer(&XGpioGlobalTimer, 1);
		print_debug(DEBUG_MAIN, "[MAIN] Reset Timer SW: %ld ns\n", u32Timer5 * HW_CLOCK_PERIOD);
		startTimer(&XGpioGlobalTimer, 1);

//		polyvec_basemul_acc_montgomery(&r, &r1, &r2);
		polyvec_basemul_acc_montgomery_sw(&r, &r1, &r2);

		stopTimer(&XGpioGlobalTimer, 1);
		u32Timer5 = getTimer(&XGpioGlobalTimer, 1);

		resetTimer(&XGpioGlobalTimer, 1);
		u32 u32Timer6 = getTimer(&XGpioGlobalTimer, 1);
		print_debug(DEBUG_MAIN, "[MAIN] Reset Timer SW: %ld ns\n", u32Timer6 * HW_CLOCK_PERIOD);
		startTimer(&XGpioGlobalTimer, 1);

//		polyvec_basemul_acc_montgomery(&r, &r1, &r2);
		polyvec_basemul_acc_montgomery_hw(&r3, &r1, &r2);

		stopTimer(&XGpioGlobalTimer, 1);
		u32Timer6 = getTimer(&XGpioGlobalTimer, 1);

//		for (int i = 0; i < 16; i++)
//		{
//			print_debug(DEBUG_MAIN, "memBram0[%d]: 0x%08lx\n", i, memoryBram0[i]);
//		}
//		for (int i = 128; i < 134; i++)
//		{
//			print_debug(DEBUG_MAIN, "memBram0[%d]: 0x%08lx\n", i, memoryBram0[i]);
//		}
//		for (int i = 256; i < 272; i++)
//		{
//			print_debug(DEBUG_MAIN, "memBram0[%d]: 0x%08lx\n", i, memoryBram0[i]);
//		}
//		for (int i = 384; i < 400; i++)
//		{
//			print_debug(DEBUG_MAIN, "memBram0[%d]: 0x%08lx\n", i, memoryBram0[i]);
//		}
//		for (int i = 0; i < 128; i++)
//		{
//			print_debug(DEBUG_MAIN, "memBram1[%d]: 0x%08lx\n", i, memoryBram1[i]);
//		}
//		for (int i = 0; i < 16; i++)
//		{
//			print_debug(DEBUG_MAIN, "r[%d]: 0x%04x\n", i, r.coeffs[i]);
//		}
		print_debug(DEBUG_MAIN, "[MAIN] Timer SW: %ld ns\n", u32Timer5 * HW_CLOCK_PERIOD);
		print_debug(DEBUG_MAIN, "[MAIN] Timer HW: %ld ns\n", u32Timer6 * HW_CLOCK_PERIOD);

		for (int i = 0; i < 256; i++)
		{
			if(r.coeffs[i] != r3.coeffs[i])
			{
				print_debug(DEBUG_MAIN, "Error at r[%d]: 0x%04x and r3[%d]: 0x%04x\n", i, r.coeffs[i], i, r3.coeffs[i]);
			}
		}
		if(memcmp(&r, &r3, 512) != 0)
		{
			print_debug(DEBUG_MAIN, "[MAIN] Error!\n");
//			exit(0);
		}
		else
			print_debug(DEBUG_MAIN, "[MAIN] Ok!\n");

#endif

//		sleep(5);
	}

	return XST_SUCCESS;
}

#if defined(XPAR_UARTNS550_0_BASEADDR)
/*****************************************************************************/
/*
*
* Uart16550 setup routine, need to set baudrate to 9600, and data bits to 8
*
* @param	None
*
* @return	None
*
* @note		None.
*
******************************************************************************/
static void Uart550_Setup(void)
{

	/* Set the baudrate to be predictable
	 */
	XUartNs550_SetBaud(XPAR_UARTNS550_0_BASEADDR,
			XPAR_XUARTNS550_CLOCK_HZ, 9600);

	XUartNs550_SetLineControlReg(XPAR_UARTNS550_0_BASEADDR,
			XUN_LCR_8_DATA_BITS);

}
#endif

