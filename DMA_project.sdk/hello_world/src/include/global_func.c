/*
 * global_func.c
 *
 *  Created on: 5 de fev de 2021
 *      Author: vinicius
 */

#include "global_def.h"

//////////////////////////////////////////////
//
//	Fraction to integrer
//
//////////////////////////////////////////////
int XAdcFractionToInt(float FloatNum)
{
	float Temp;

	Temp = FloatNum;
	if (FloatNum < 0) {
		Temp = -(FloatNum);
	}

	return( ((int)((Temp -(float)((int)Temp)) * (1000.0f))));
}

//////////////////////////////////////////////
//
//	Print temperature
//
//////////////////////////////////////////////
void getChipTemperature()
{
	static XAdcPs XAdcInst;
	XAdcPs_Config *ConfigPtr;
	u32 TempRawData;
	float TempData;
	XAdcPs *XAdcInstPtr = &XAdcInst;

	ConfigPtr = XAdcPs_LookupConfig(XADC_DEVICE_ID);
	XAdcPs_CfgInitialize(XAdcInstPtr, ConfigPtr, ConfigPtr->BaseAddress);
	XAdcPs_SelfTest(XAdcInstPtr);
	XAdcPs_SetSequencerMode(XAdcInstPtr, XADCPS_SEQ_MODE_SAFE);
	TempRawData = XAdcPs_GetAdcData(XAdcInstPtr, XADCPS_CH_TEMP);
	srand(TempRawData); //Get a random seed here!
	TempData = XAdcPs_RawToTemperature(TempRawData);
	print_debug(DEBUG_MAIN, "[MAIN] The Current Temperature is %0d.%03d Centigrades\n", (int)(TempData), XAdcFractionToInt(TempData));
}

//////////////////////////////////////////////
//
//	LED initialize
//
//////////////////////////////////////////////
void ledInit(XGpioPs * Gpio)
{
	//---- Blink led ----
	XGpioPs_Config *GPIOConfigPtr;
	GPIOConfigPtr = XGpioPs_LookupConfig(XPAR_PS7_GPIO_0_DEVICE_ID);
	XGpioPs_CfgInitialize(Gpio, GPIOConfigPtr, GPIOConfigPtr ->BaseAddr);
	XGpioPs_SetDirectionPin(Gpio, ledpin, 1);
	XGpioPs_SetOutputEnablePin(Gpio, ledpin, 1);
}

//////////////////////////////////////////////
//
//	Configure Kyber K
//
//////////////////////////////////////////////
void configKyberK(XGpio_Config * pConfigStruct, XGpio * pGpioStruct, uint8_t ui8DeviceId, uint8_t ui8Channel)
{
	pConfigStruct = XGpio_LookupConfig(ui8DeviceId);
	XGpio_CfgInitialize(pGpioStruct, pConfigStruct, pConfigStruct->BaseAddress);
	XGpio_DiscreteWrite(pGpioStruct, ui8Channel, KYBER_K); //Set enable bit and reset bit low.
}

//////////////////////////////////////////////
//
//	Configure timer
//
//////////////////////////////////////////////
void configTimer(XGpio_Config * pConfigStruct, XGpio * pGpioStruct, uint8_t ui8DeviceId, uint8_t ui8Channel)
{
	pConfigStruct = XGpio_LookupConfig(ui8DeviceId);
	XGpio_CfgInitialize(pGpioStruct, pConfigStruct, pConfigStruct->BaseAddress);
	XGpio_DiscreteWrite(pGpioStruct, ui8Channel, 0x0); //Set enable bit and reset bit low.
}

//////////////////////////////////////////////
//
//	Reset hardware timer
//
//////////////////////////////////////////////
void resetTimer(XGpio * pStruct, uint8_t ui8Channel)
{
	XGpio_DiscreteWrite(pStruct, ui8Channel, 0x0); //Set reset bit low.
	XGpio_DiscreteWrite(pStruct, ui8Channel, 0x1); //Set reset bit high.
	XGpio_DiscreteWrite(pStruct, ui8Channel, 0x0); //Set reset bit low.
}

//////////////////////////////////////////////
//
//	Start hardware timer
//
//////////////////////////////////////////////
void startTimer(XGpio * pStruct, uint8_t ui8Channel)
{
	XGpio_DiscreteWrite(pStruct, ui8Channel, 0x2); //Set enable bit high.
}

//////////////////////////////////////////////
//
//	Stop hardware timer
//
//////////////////////////////////////////////
void stopTimer(XGpio * pStruct, uint8_t ui8Channel)
{
	XGpio_DiscreteWrite(pStruct, ui8Channel, 0x0); //Set enable bit low.
}

//////////////////////////////////////////////
//
//	Get hardware timer
//
//////////////////////////////////////////////
u32 getTimer(XGpio * pStruct, uint8_t ui8Channel)
{
	return XGpio_DiscreteRead(pStruct, ui8Channel);
}

//////////////////////////////////////////////
//
//	Float to integer and fraction
//
//////////////////////////////////////////////
void floatToIntegers(double dValue, u32 * u32Integer, u32 * u32Fraction)
{
	*u32Integer = dValue;
	*u32Fraction = (dValue - *u32Integer) * 1000;
}
