################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/include/aes256ctr.c \
../src/include/cbd.c \
../src/include/fips202.c \
../src/include/global_func.c \
../src/include/indcpa.c \
../src/include/kem.c \
../src/include/ntt.c \
../src/include/poly.c \
../src/include/polyvec.c \
../src/include/randombytes.c \
../src/include/reduce.c \
../src/include/sha256.c \
../src/include/sha512.c \
../src/include/symmetric-aes.c \
../src/include/symmetric-shake.c \
../src/include/test_kem.c \
../src/include/verify.c 

OBJS += \
./src/include/aes256ctr.o \
./src/include/cbd.o \
./src/include/fips202.o \
./src/include/global_func.o \
./src/include/indcpa.o \
./src/include/kem.o \
./src/include/ntt.o \
./src/include/poly.o \
./src/include/polyvec.o \
./src/include/randombytes.o \
./src/include/reduce.o \
./src/include/sha256.o \
./src/include/sha512.o \
./src/include/symmetric-aes.o \
./src/include/symmetric-shake.o \
./src/include/test_kem.o \
./src/include/verify.o 

C_DEPS += \
./src/include/aes256ctr.d \
./src/include/cbd.d \
./src/include/fips202.d \
./src/include/global_func.d \
./src/include/indcpa.d \
./src/include/kem.d \
./src/include/ntt.d \
./src/include/poly.d \
./src/include/polyvec.d \
./src/include/randombytes.d \
./src/include/reduce.d \
./src/include/sha256.d \
./src/include/sha512.d \
./src/include/symmetric-aes.d \
./src/include/symmetric-shake.d \
./src/include/test_kem.d \
./src/include/verify.d 


# Each subdirectory must supply rules for building sources it contributes
src/include/%.o: ../src/include/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -I../../hello_world_bsp/ps7_cortexa9_0/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


