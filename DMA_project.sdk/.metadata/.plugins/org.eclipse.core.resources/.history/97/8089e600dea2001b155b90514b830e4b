/******************************************************************************
*
* Copyright (C) 2010 - 2019 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/
/*****************************************************************************/
/**
 *
 * @file xaxidma_example_sg_poll.c
 *
 * This file demonstrates how to use the xaxidma driver on the Xilinx AXI
 * DMA core (AXIDMA) to transfer packets in polling mode when the AXIDMA
 * core is configured in Scatter Gather Mode.
 *
 * This code assumes a loopback hardware widget is connected to the AXI DMA
 * core for data packet loopback.
 *
 * To see the debug print, you need a Uart16550 or uartlite in your system,
 * and please set "-DDEBUG" in your compiler options. You need to rebuild your
 * software executable.
 *
 * Make sure that MEMORY_BASE is defined properly as per the HW system. The
 * h/w system built in Area mode has a maximum DDR memory limit of 64MB. In
 * throughput mode, it is 512MB.  These limits are need to ensured for
 * proper operation of this code.
 *
 *
 * <pre>
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -------------------------------------------------------
 * 1.00a jz   05/17/10 First release
 * 2.00a jz   08/10/10 Second release, added in xaxidma_g.c, xaxidma_sinit.c,
 *                     updated tcl file, added xaxidma_porting_guide.h, removed
 *                     workaround for endianness
 * 4.00a rkv  02/22/11 Name of the file has been changed for naming consistency
 *       	       	   Added interrupt support for ARM.
 * 5.00a srt  03/05/12 Added Flushing and Invalidation of Caches to fix CRs
 *		       		   648103, 648701.
 *		       		   Added V7 DDR Base Address to fix CR 649405.
 * 6.00a srt  03/27/12 Changed API calls to support MCDMA driver.
 * 7.00a srt  06/18/12 API calls are reverted back for backward compatibility.
 * 7.01a srt  11/02/12 Buffer sizes (Tx and Rx) are modified to meet maximum
 *		       DDR memory limit of the h/w system built with Area mode
 * 7.02a srt  03/01/13 Updated DDR base address for IPI designs (CR 703656).
 * 9.1   adk  01/07/16 Updated DDR base address for Ultrascale (CR 799532) and
 *		       removed the defines for S6/V6.
 * 9.2   vak  15/04/16 Fixed compilation warnings in th example
 * 9.3   ms   01/23/17 Modified xil_printf statement in main function to
 *                     ensure that "Successfully ran" and "Failed" strings are
 *                     available in all examples. This is a fix for CR-965028.
 * 9.9   rsp  01/21/19 Fix use of #elif check in deriving DDR_BASE_ADDR.
 * </pre>
 *
 * ***************************************************************************
 */
/***************************** Include Files *********************************/
#include "xaxidma.h"
#include "xparameters.h"
#include "xdebug.h"
#include "sleep.h"
#include "xgpio.h"
#include "include/global_def.h"
#include "include/poly.h"
#include "include/polyvec.h"
#include "include/fips202.h"

#ifdef __aarch64__
#include "xil_mmu.h"
#endif

#if defined(XPAR_UARTNS550_0_BASEADDR)
#include "xuartns550_l.h"       /* to use uartns550 */
#endif

#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif

/******************** Constant Definitions **********************************/

/*
 * Device hardware build related constants.
 */

#define DMA_DEV_ID		XPAR_AXIDMA_0_DEVICE_ID

#ifdef XPAR_AXI_7SDDR_0_S_AXI_BASEADDR
#define DDR_BASE_ADDR		XPAR_AXI_7SDDR_0_S_AXI_BASEADDR
#elif defined (XPAR_MIG7SERIES_0_BASEADDR)
#define DDR_BASE_ADDR	XPAR_MIG7SERIES_0_BASEADDR
#elif defined (XPAR_MIG_0_BASEADDR)
#define DDR_BASE_ADDR	XPAR_MIG_0_BASEADDR
#elif defined (XPAR_PSU_DDR_0_S_AXI_BASEADDR)
#define DDR_BASE_ADDR	XPAR_PSU_DDR_0_S_AXI_BASEADDR
#endif

#ifndef DDR_BASE_ADDR
#warning CHECK FOR THE VALID DDR ADDRESS IN XPARAMETERS.H, \
		 DEFAULT SET TO 0x01000000
#define MEM_BASE_ADDR		0x01000000
#else
#define MEM_BASE_ADDR		(DDR_BASE_ADDR + 0x1000000)
#endif

#define TX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00100000)
#define RX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00300000)
#define RX_BUFFER_HIGH		(MEM_BASE_ADDR + 0x004FFFFF)

#define MAX_PKT_LEN		0x4FA0 //Maximum value in bytes
//#define MAX_PKT_LEN		0x40 //Maximum value in bytes
#define MARK_UNCACHEABLE        0x701

#define TEST_START_VALUE	0xC
#define NUMBER_OF_TRANSFERS	10

/**************************** Type Definitions *******************************/
XGpio_Config * XGpioConfigDma;
XGpio XGpioDma;

/***************** Macros (Inline Functions) Definitions *********************/


/************************** Function Prototypes ******************************/
#if defined(XPAR_UARTNS550_0_BASEADDR)
static void Uart550_Setup(void);
#endif

static int RxSetup(XAxiDma * AxiDmaInstPtr);
static int TxSetup(XAxiDma * AxiDmaInstPtr);
static int SendPacket(XAxiDma * AxiDmaInstPtr, u32 * pu32Buffer, size_t sBytes);
static int GetData(u32 * pu32Buffer, size_t sBytes);
static int ReceivePacket(XAxiDma * AxiDmaInstPtr, u32 * pu32Buffer, size_t sBytes);

extern XGpio_Config * XGpioConfigPtrGlobalTimer;
extern XGpio XGpioGlobalTimer;

extern XGpio_Config * XGpioConfigTomontAndReduce;
extern XGpio XGpioTomontAndReduce;

/************************** Variable Definitions *****************************/
/*
 * Device instance definitions
 */
XAxiDma AxiDma;

/*****************************************************************************/
/**
*
* Main function
*
* This function is the main entry of the tests on DMA core. It sets up
* DMA engine to be ready to receive and send packets, then a packet is
* transmitted and will be verified after it is received via the DMA loopback
* widget.
*
* @param	None
*
* @return
*		- XST_SUCCESS if test passes
*		- XST_FAILURE if test fails.
*
* @note		None.
*
******************************************************************************/
int main(void)
{
	XAxiDma_Config *CfgPtr;
	int Status;
//	int Tries = NUMBER_OF_TRANSFERS;
	int Index;
	u8 *TxBufferPtr;
	u8 *RxBufferPtr;
	u8 Value;

	TxBufferPtr = (u8 *)TX_BUFFER_BASE ;
	RxBufferPtr = (u8 *)RX_BUFFER_BASE;

#if defined(XPAR_UARTNS550_0_BASEADDR)

	Uart550_Setup();

#endif

	xil_printf("\r\n--- Entering main() --- \r\n");

#ifdef __aarch64__
	Xil_SetTlbAttributes(TX_BD_SPACE_BASE, MARK_UNCACHEABLE);
	Xil_SetTlbAttributes(RX_BD_SPACE_BASE, MARK_UNCACHEABLE);
#endif
	XGpioConfigDma = XGpio_LookupConfig(XPAR_AXI_GPIO_0_BASEADDR);
	XGpio_CfgInitialize(&XGpioDma, XGpioConfigDma, XGpioConfigDma->BaseAddress);

	//---- Configure timers ----
	configTimer(XGpioConfigPtrGlobalTimer, &XGpioGlobalTimer, XPAR_AXI_GPIO_1_DEVICE_ID, 1);

	//Poly tomont and reduce
	XGpioConfigTomontAndReduce = XGpio_LookupConfig(XPAR_AXI_GPIO_2_DEVICE_ID);
	XGpio_CfgInitialize(&XGpioTomontAndReduce, XGpioConfigTomontAndReduce, XGpioConfigTomontAndReduce->BaseAddress);

	//Values < 8192 (or <= 8160) and must be 32 bytes aligned (size/32 = integer).
	size_t sTxSizePacket = 512; //bytes
	size_t sRxSizePacket = 512; //bytes

	CfgPtr = XAxiDma_LookupConfig(DMA_DEV_ID);
	if (!CfgPtr) {
		xil_printf("No config found for %d\r\n", DMA_DEV_ID);
		return XST_FAILURE;
	}

	Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtr);
	if (Status != XST_SUCCESS) {
		xil_printf("Initialization failed %d\r\n", Status);
		return XST_FAILURE;
	}

	if(XAxiDma_HasSg(&AxiDma)){
		xil_printf("Device configured as SG mode \r\n");
		return XST_FAILURE;
	}

	/* Disable interrupts, we use polling mode
	 */
	XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
						XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
						XAXIDMA_DMA_TO_DEVICE);

	int j = 0x0;

	while(1)
	{
		j++;

		//Poly tomont test
		poly r1;
		poly r2;
		poly r3 = {0};
		poly r_test = {0};
		for(int i = 0; i < 256; i = i + 4)
		{
			r_test.coeffs[i + 0] = 0x03fb + j;
			r_test.coeffs[i + 1] = 0x062e + j;
			r_test.coeffs[i + 2] = 0x0593 + j;
			r_test.coeffs[i + 3] = 0x039b + j;
		}

		for(int i = 0; i < 8; i++)
		{
			xil_printf("r_test.coeffs[%d]: %x\r\n", i, r_test.coeffs[i]);
		}

		memcpy(&r1, &r_test, 512);
		memcpy(&r2, &r_test, 512);

		resetTimer(&XGpioGlobalTimer, 1);
		u32 u32Timer1 = getTimer(&XGpioGlobalTimer, 1);
		xil_printf("[MAIN] Reset Timer HW: %ld ns\n", u32Timer1 * 10);
		startTimer(&XGpioGlobalTimer, 1);

		Xil_DCacheFlushRange((UINTPTR)&r1, sTxSizePacket);

		//Configure RX
		 XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) &r1,
				sRxSizePacket, XAXIDMA_DEVICE_TO_DMA);

		//Configure TX
		XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) &r1,
				sTxSizePacket, XAXIDMA_DMA_TO_DEVICE);

		while (XAxiDma_Busy(&AxiDma,XAXIDMA_DMA_TO_DEVICE)) {
				/* Wait */
		}

		//Start flag up
		XGpio_DiscreteWrite(&XGpioTomontAndReduce, 1, 0x1);

		//Send size to be read.
		XGpio_DiscreteWrite(&XGpioDma, 2, sRxSizePacket >> 2); //Number of words, not bytes.

		//Read busy signal
		u32 u32ReadGpio = XGpio_DiscreteRead(&XGpioTomontAndReduce, 1);
		while(u32ReadGpio == 1)
			u32ReadGpio = XGpio_DiscreteRead(&XGpioTomontAndReduce, 1);

		//DMA packet reception flag up
		XGpio_DiscreteWrite(&XGpioDma, 1, 0x1);

		while (XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA)) {
				/* Wait */
		}

		Xil_DCacheFlushRange((UINTPTR)&r1, sRxSizePacket);

		//DMA packet reception flag down
		XGpio_DiscreteWrite(&XGpioDma, 1, 0x0);

		//Start flag down
		XGpio_DiscreteWrite(&XGpioTomontAndReduce, 1, 0x0);

		stopTimer(&XGpioGlobalTimer, 1);
		u32Timer1 = getTimer(&XGpioGlobalTimer, 1);

		//Received
		for(int i = 0; i < 8; i++)
		{
			xil_printf("r1.coeffs[%d]: %x\r\n", i, r1.coeffs[i]);
		}

		resetTimer(&XGpioGlobalTimer, 1);
		u32 u32Timer2 = getTimer(&XGpioGlobalTimer, 1);
		print_debug(DEBUG_MAIN, "[MAIN] Reset Timer SW: %ld ns\n", u32Timer2 * 10);
		startTimer(&XGpioGlobalTimer, 1);

		poly_tomont_sw(&r2);

		stopTimer(&XGpioGlobalTimer, 1);
		u32Timer2 = getTimer(&XGpioGlobalTimer, 1);
//		for(int i = 0; i < 4; i++)
//		{
//			print_debug(DEBUG_MAIN, "[MAIN] poly2 result[%d]: 0x%04x\n", i, r2.coeffs[i]);
//		}
		for(int i = 0; i < 8; i++)
		{
			xil_printf("r2.coeffs[%d]: %x\r\n", i, r2.coeffs[i]);
		}

		if(memcmp(&r1, &r2, 512) != 0)
		{
			print_debug(DEBUG_MAIN, "[MAIN] Error!\n");
			exit(0);
		}
		else
			print_debug(DEBUG_MAIN, "[MAIN] Ok!\n");

		print_debug(DEBUG_MAIN, "[MAIN] Timer SW: %ld ns\n", u32Timer2 * HW_CLOCK_PERIOD);
		print_debug(DEBUG_MAIN, "[MAIN] Timer HW: %ld ns\n", u32Timer1 * HW_CLOCK_PERIOD);


		sleep(1);
	}

	return XST_SUCCESS;
}

#if defined(XPAR_UARTNS550_0_BASEADDR)
/*****************************************************************************/
/*
*
* Uart16550 setup routine, need to set baudrate to 9600, and data bits to 8
*
* @param	None
*
* @return	None
*
* @note		None.
*
******************************************************************************/
static void Uart550_Setup(void)
{

	/* Set the baudrate to be predictable
	 */
	XUartNs550_SetBaud(XPAR_UARTNS550_0_BASEADDR,
			XPAR_XUARTNS550_CLOCK_HZ, 9600);

	XUartNs550_SetLineControlReg(XPAR_UARTNS550_0_BASEADDR,
			XUN_LCR_8_DATA_BITS);

}
#endif

